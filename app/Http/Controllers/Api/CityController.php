<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\City;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = City::get();
        return response()->json($list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'city'            => 'required|unique:city',
            'city_short'      => 'required|unique:city',
            'state_id'        => 'required',

        ]);
        
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
			$store = $request->all();
            $data = new City($store);
			if($data->save()) {
				$re = [
                    'status' => true,
                    'message'	=> 'Added Successfully.'
                ]; 
			}else{
				$re = [
                    'status' => false,
                    'message'	=> 'No record(s) found.'
                ];
			}
		}


        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {
        
        $validator = Validator::make($request->all(), [
            'city'             => 'required|unique:city,city,'.$city->id,
            'city_short'       => 'required|unique:city,city_short,'.$city->id,
            'state_id'         => 'required',
        ]);
        
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
			$store = $request->all();
            $data = City::findOrFail($city->id);
			if($data->update($store)) {
				$re = [
                    'status' => true,
                    'message'	=> 'Added Successfully.'
                ]; 
			}else{
				$re = [
                    'status' => false,
                    'message'	=> 'No record(s) found.'
                ];
			}
		}


        return response()->json($re);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        
        $del   = City::findOrFail($city->id);
        if($del->delete()) {
            $re = [
                'status' => true,
                'message'	=> 'Delete Successfully.'
            ]; 
        }else{
            $re = [
                'status' => false,
                'message'	=> 'Please try again'
            ];
        }
        return response()->json($re);
    }
}
