<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Country::get();
        return response()->json($list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'country'             => 'required|unique:country',
            'country_short'       => 'required|unique:country'
        ]);
        
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
			$store = $request->all();
            $data = new Country($store);
			if($data->save()) {
				$re = [
                    'status' => true,
                    'message'	=> 'Added Successfully.'
                ]; 
			}else{
				$re = [
                    'status' => false,
                    'message'	=> 'No record(s) found.'
                ];
			}
		}


        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country)
    {
        
        $validator = Validator::make($request->all(), [
            'country'             => 'required|unique:country,country,'.$country->id,
            'country_short'       => 'required|unique:country,country_short,'.$country->id,
        ]);
        
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
			$store = $request->all();
            $data = Country::findOrFail($country->id);
			if($data->update($store)) {
				$re = [
                    'status' => true,
                    'message'	=> 'Added Successfully.'
                ]; 
			}else{
				$re = [
                    'status' => false,
                    'message'	=> 'No record(s) found.'
                ];
			}
		}


        return response()->json($re);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        
        $del = Country::findOrFail($country->id);
        
        if($del->delete()) {
            $re = [
                'status' => true,
                'message'	=> 'Delete Successfully.'
            ]; 
        }else{
            $re = [
                'status' => false,
                'message'	=> 'Please try again'
            ];
        }
        return response()->json($re);
    }
}
