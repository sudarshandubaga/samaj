<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $re = News::get();
        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $validator = Validator::make($request->all(), [
            'title'        => 'required',
            'description'  => 'required',
        ]);
        

        if($validator->fails()){
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $news = $request->all();
            if($request->hasFile('image'))  
            { 
                $image        = $request->file('image');
                $filename     = uniqid() . '.' . $image->getClientOriginalExtension();
                $image_resize = Image::make($image->getRealPath());              
                $image_resize->resize(150, 175);
                $image_resize->save(public_path('imgs/news/' .$filename));
                $news['image']   = $filename;
            }
            $news['type'] = 'news';
            $data = new News($news); 
            if($data->save()) {
				$re = [
                    'status' => true,
                    'message'	=> 'Added Successfully.'
                ]; 
			}else{
				$re = [
                    'status' => false,
                    'message'	=> 'No record(s) found.'
                ];
			}
        }
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        
        $validator = Validator::make($request->all(), [
            'title'             => 'required',
            'description'       => 'required',
        ]);
        if($validator->fails()){
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $data = News::findOrFail($news->id);
        
            $news = $request->all();
            $news['type'] = 'news';
            if($request->hasFile('image'))  
            { 
                $image        = $request->file('image');
                $filename     = uniqid() . '.' . $image->getClientOriginalExtension();
                $image_resize = Image::make($image->getRealPath());              
                $image_resize->resize(150, 175);
                $image_resize->save(public_path('imgs/news/' .$filename));
                $news['image']   = $filename;
            }
            
            
            
            if($data->update($news)) {
				$re = [
                    'status' => true,
                    'message'	=> 'Update Successfully.'
                ]; 
			}else{
				$re = [
                    'status' => false,
                    'message'	=> 'No record(s) found.'
                ];
			}
        }
        return response()->json($re);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $del = News::findOrFail($news->id);
        if($del->delete()) {
            $re = [
                'status' => true,
                'message'	=> 'Delete Successfully.'
            ]; 
        }else{
            $re = [
                'status' => false,
                'message'	=> 'Please try again'
            ];
        }
        return response()->json($re);
    }
}
