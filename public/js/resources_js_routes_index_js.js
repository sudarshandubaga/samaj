(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_routes_index_js"],{

/***/ "./resources/js/routes/index.js":
/*!**************************************!*\
  !*** ./resources/js/routes/index.js ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.esm.js");
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.esm.js");


vue__WEBPACK_IMPORTED_MODULE_0__.default.use(vue_router__WEBPACK_IMPORTED_MODULE_1__.default);
var routes = [{
  path: '*',
  name: 'Dasboard',
  component: function component() {
    return __webpack_require__.e(/*! import() | about */ "about").then(__webpack_require__.bind(__webpack_require__, /*! ../components/Pages/Dashboard */ "./resources/js/components/Pages/Dashboard.vue"));
  }
}];
var router = new vue_router__WEBPACK_IMPORTED_MODULE_1__.default({
  mode: 'history',
  base: '/smj-control',
  routes: routes
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (router);

/***/ })

}]);