<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\State;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = State::get();
        return response()->json($list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'state'             => 'required|unique:state',
            'state_short'       => 'required|unique:state',
            'country_id'        => 'required',
        ]);
        
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
			$store = $request->all();
            $data = new State($store);
			if($data->save()) {
				$re = [
                    'status' => true,
                    'message'	=> 'Added Successfully.'
                ]; 
			}else{
				$re = [
                    'status' => false,
                    'message'	=> 'No record(s) found.'
                ];
			}
		}


        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, State $state)
    {
        
        $validator = Validator::make($request->all(), [
            'state'             => 'required|unique:state,state,'.$state->id,
            'state_short'       => 'required|unique:state,state_short,'.$state->id,
            'country_id'        => 'required',
        ]);
        
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
			$store = $request->all();
            $data = State::findOrFail($state->id);
			if($data->update($store)) {
				$re = [
                    'status' => true,
                    'message'	=> 'Added Successfully.'
                ]; 
			}else{
				$re = [
                    'status' => false,
                    'message'	=> 'No record(s) found.'
                ];
			}
		}


        return response()->json($re);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\State  $state
     * @return \Illuminate\Http\Response
     */
    public function destroy(State $state)
    {
        
        $del = State::findOrFail($state->id);
        
        if($del->delete()) {
            $re = [
                'status' => true,
                'message'	=> 'Delete Successfully.'
            ]; 
        }else{
            $re = [
                'status' => false,
                'message'	=> 'Please try again'
            ];
        }
        return response()->json($re);
    }
}
