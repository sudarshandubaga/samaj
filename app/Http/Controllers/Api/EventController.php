<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use App\Models\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $re = Event::where('type','event')->get();
        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title'        => 'required',
            'description'  => 'required',
            'date'         => 'required',
        ]);
        

        if($validator->fails()){
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $event = $request->all();
            $event['type'] = 'event';
            if($request->hasFile('image'))  
            { 
                $image        = $request->file('image');
                $filename     = uniqid() . '.' . $image->getClientOriginalExtension();
                $image_resize = Image::make($image->getRealPath());              
                $image_resize->resize(150, 175);
                $image_resize->save(public_path('imgs/event/' .$filename));
                $event['image']   = $filename;
            }
            $data = new Event($event); 
            if($data->save()) {
				$re = [
                    'status' => true,
                    'message'	=> 'Added Successfully.'
                ]; 
			}else{
				$re = [
                    'status' => false,
                    'message'	=> 'No record(s) found.'
                ];
			}
        }
        return response()->json($re);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $validator = Validator::make($request->all(), [
            'title'             => 'required',
            'description'       => 'required',
            'date'              => 'required',
        ]);
        if($validator->fails()){
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $data = Event::findOrFail($event->id);
        
            $event = $request->all();
            $event['type'] = 'event';
            if($request->hasFile('image'))  
            { 
                $image        = $request->file('image');
                $filename     = uniqid() . '.' . $image->getClientOriginalExtension();
                $image_resize = Image::make($image->getRealPath());              
                $image_resize->resize(150, 175);
                $image_resize->save(public_path('imgs/event/' .$filename));
                $event['image']   = $filename;
            }
            
            
            
            if($data->update($event)) {
				$re = [
                    'status' => true,
                    'message'	=> 'Update Successfully.'
                ]; 
			}else{
				$re = [
                    'status' => false,
                    'message'	=> 'No record(s) found.'
                ];
			}
        }
        return response()->json($re);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $del = Event::findOrFail($event->id);
        if($del->delete()) {
            $re = [
                'status' => true,
                'message'	=> 'Delete Successfully.'
            ]; 
        }else{
            $re = [
                'status' => false,
                'message'	=> 'Please try again'
            ];
        }
        return response()->json($re);
    }
}
