<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prayers', function (Blueprint $table) {
            $table->id();
            $table->string('prayer_title');
            $table->text('prayer_description');
            $table->enum('file_type', ['content', 'video', 'audio'])->default('content');
            $table->string('prayer_file')->nullable();
            $table->unsignedBigInteger('samaj_id')->nullable();
            $table->foreign('samaj_id')->references('id')->on('samaj');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prayers');
    }
}
