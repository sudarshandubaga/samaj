<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Samaj extends Model
{
    // use HasFactory;
    protected $table   = "samaj";
    protected $guarded = []; 
}
