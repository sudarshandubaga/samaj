<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $re = Role::get();
        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:roles',
        ]);

        if($validator->fails()){
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $store = $request->all();
            $data = new Role($store);
			if($data->save()) {
				$re = [
                    'status' => true,
                    'message'	=> 'Added Successfully.'
                ]; 
			}else{
				$re = [
                    'status' => false,
                    'message'	=> 'No record(s) found.'
                ];
			}
        }
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|unique:roles,name,'.$role->id,
        ]);

        if($validator->fails()){
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $data = Role::findOrFail($role->id);
            $update = $request->all();
            if($data->update($update)){
                $re = [
                    'status'  => true,
                    'message' => 'Updated successfully',
                ];
            } else {
                $re = [
                    'status'  => false,
                    'message' => 'Please try again'
                ];
            }
        }
        return response()->json($re);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $del = Role::findOrFail($role->id);

        if($del->delete()) {
            $re = [
                'status' => true,
                'message'	=> 'Delete Successfully.'
            ]; 
        }else{
            $re = [
                'status' => false,
                'message'	=> 'Please try again'
            ];
        }
        return response()->json($re);
    }
}
