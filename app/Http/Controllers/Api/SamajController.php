<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Samaj;
use Illuminate\Http\Request;

class SamajController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $re = Samaj::get();
        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:samaj',
        ]);

        if($validator->fails()){
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $store = $request->all();
            $data = new Samaj($store);
			if($data->save()) {
				$re = [
                    'status' => true,
                    'message'	=> 'Added Successfully.'
                ]; 
			}else{
				$re = [
                    'status' => false,
                    'message'	=> 'No record(s) found.'
                ];
			}
        }
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Samaj  $samaj
     * @return \Illuminate\Http\Response
     */
    public function show(Samaj $samaj)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Samaj  $samaj
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Samaj $samaj)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|unique:samaj,name,'.$samaj->id,
        ]);

        if($validator->fails()){
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $data = Samaj::findOrFail($samaj->id);
            $update = $request->all();
            if($data->update($update)){
                $re = [
                    'status'  => true,
                    'message' => 'Updated successfully',
                ];
            } else {
                $re = [
                    'status'  => false,
                    'message' => 'Please try again'
                ];
            }
        }
        return response()->json($re);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Samaj  $samaj
     * @return \Illuminate\Http\Response
     */
    public function destroy(Samaj $samaj)
    {
        $del = Samaj::findOrFail($samaj->id);

        if($del->delete()) {
            $re = [
                'status' => true,
                'message'	=> 'Delete Successfully.'
            ]; 
        }else{
            $re = [
                'status' => false,
                'message'	=> 'Please try again'
            ];
        }
        return response()->json($re);
    }
}
