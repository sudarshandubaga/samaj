import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Dashboard',
        component: () => import('../components/Pages/Dashboard'),
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('../components/Pages/Dashboard'),
    }
]

const router = new VueRouter({
    mode: 'history',
    base: '/smj-control',
    routes
})

export default router
